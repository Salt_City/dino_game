use ggez::graphics;
use ggez::{Context, GameResult};

use glam::Vec2;

pub struct JungleLevel {
    pub flat_grass_1: graphics::Image,
    pub flat_grass_2: graphics::Image
    // pub flat_grass_3: graphics::Image,
    // pub stone_wall_left: graphics::Image,
    // pub inner_ground_1: graphics::Image,
}

impl JungleLevel {
    pub fn new(ctx: &mut Context) -> GameResult<JungleLevel> {
        let flat_grass_1 = graphics::Image::new(ctx, "/level/jungle/flat_grass_1.png")?;
        let flat_grass_2 = graphics::Image::new(ctx, "/level/jungle/flat_grass_2.png")?;

        Ok(
            JungleLevel{
                flat_grass_1,
                flat_grass_2
            }
        )
    }
}

pub struct Tile {
    pub position: Vec2,
    pub image: graphics::Image,
    pub param: graphics::DrawParam
}

impl Tile {
    pub fn new(pos: Vec2, image: graphics::Image) -> Tile {
        let param = graphics::DrawParam::new()
            .dest(pos);
        
        Tile {
            position: pos,
            image,
            param
        }
    }
}

pub trait Level {
    fn new(ctx: &mut Context) -> Self;
    
    fn draw(&mut self, ctx: &mut Context);

}