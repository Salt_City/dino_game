use ggez::graphics::{self};
use ggez::{Context, GameResult};

use glam::*;

use std::time::SystemTime;

pub enum CurrentAction {
    RUNNING,
    WALKING,
    ATTACKING,
    SPITTING,
    IDLE,
    JUMPING
}

pub struct Dilopo {
    pub run: [graphics::Image; 8],
    pub walk: [graphics::Image; 10],
    pub attack: [graphics::Image; 10],
    pub spit: [graphics::Image; 10],
    pub idle: [graphics::Image; 10],
    pub dead: [graphics::Image; 10],
    pub current_action: CurrentAction,
    pub run_index: usize,
    pub walk_index: usize,
    pub attack_index: usize,
    pub spit_index: usize,
    pub idle_index: usize,
    pub dead_index: usize,
    pub position: Vec2,
    pub velocity: Vec2,
    pub venom: Vec<Venom>
}

impl Dilopo {
    pub fn new(ctx: &mut Context) -> GameResult<Dilopo> {
        let run: [graphics::Image; 8] = [
            graphics::Image::new(ctx, "/dilopo/run_1.png")?,
            graphics::Image::new(ctx, "/dilopo/run_2.png")?,
            graphics::Image::new(ctx, "/dilopo/run_3.png")?,
            graphics::Image::new(ctx, "/dilopo/run_4.png")?,
            graphics::Image::new(ctx, "/dilopo/run_5.png")?,
            graphics::Image::new(ctx, "/dilopo/run_6.png")?,
            graphics::Image::new(ctx, "/dilopo/run_7.png")?,
            graphics::Image::new(ctx, "/dilopo/run_8.png")?,
        ];

        let walk: [graphics::Image; 10] = [
            graphics::Image::new(ctx, "/dilopo/walk_1.png")?,
            graphics::Image::new(ctx, "/dilopo/walk_2.png")?,
            graphics::Image::new(ctx, "/dilopo/walk_3.png")?,
            graphics::Image::new(ctx, "/dilopo/walk_4.png")?,
            graphics::Image::new(ctx, "/dilopo/walk_5.png")?,
            graphics::Image::new(ctx, "/dilopo/walk_6.png")?,
            graphics::Image::new(ctx, "/dilopo/walk_7.png")?,
            graphics::Image::new(ctx, "/dilopo/walk_8.png")?,
            graphics::Image::new(ctx, "/dilopo/walk_9.png")?,
            graphics::Image::new(ctx, "/dilopo/walk_10.png")?, 
        ];

        let attack: [graphics::Image; 10] = [
            graphics::Image::new(ctx, "/dilopo/attack_1.png")?,
            graphics::Image::new(ctx, "/dilopo/attack_2.png")?,
            graphics::Image::new(ctx, "/dilopo/attack_3.png")?,
            graphics::Image::new(ctx, "/dilopo/attack_4.png")?,
            graphics::Image::new(ctx, "/dilopo/attack_5.png")?,
            graphics::Image::new(ctx, "/dilopo/attack_6.png")?,
            graphics::Image::new(ctx, "/dilopo/attack_7.png")?,
            graphics::Image::new(ctx, "/dilopo/attack_8.png")?,
            graphics::Image::new(ctx, "/dilopo/attack_9.png")?,
            graphics::Image::new(ctx, "/dilopo/attack_10.png")?,
        ];

        let spit: [graphics::Image; 10] = [
            graphics::Image::new(ctx, "/dilopo/spit_1.png")?,
            graphics::Image::new(ctx, "/dilopo/spit_2.png")?,
            graphics::Image::new(ctx, "/dilopo/spit_3.png")?,
            graphics::Image::new(ctx, "/dilopo/spit_4.png")?,
            graphics::Image::new(ctx, "/dilopo/spit_5.png")?,
            graphics::Image::new(ctx, "/dilopo/spit_6.png")?,
            graphics::Image::new(ctx, "/dilopo/spit_7.png")?,
            graphics::Image::new(ctx, "/dilopo/spit_8.png")?,
            graphics::Image::new(ctx, "/dilopo/spit_9.png")?,
            graphics::Image::new(ctx, "/dilopo/spit_10.png")?,
        ];

        let idle: [graphics::Image; 10] = [
            graphics::Image::new(ctx, "/dilopo/idle_1.png")?,
            graphics::Image::new(ctx, "/dilopo/idle_2.png")?,
            graphics::Image::new(ctx, "/dilopo/idle_3.png")?,
            graphics::Image::new(ctx, "/dilopo/idle_4.png")?,
            graphics::Image::new(ctx, "/dilopo/idle_5.png")?,
            graphics::Image::new(ctx, "/dilopo/idle_6.png")?,
            graphics::Image::new(ctx, "/dilopo/idle_7.png")?,
            graphics::Image::new(ctx, "/dilopo/idle_8.png")?,
            graphics::Image::new(ctx, "/dilopo/idle_9.png")?,
            graphics::Image::new(ctx, "/dilopo/idle_10.png")?,
        ];

        let dead: [graphics::Image; 10] = [
            graphics::Image::new(ctx, "/dilopo/dead_1.png")?,
            graphics::Image::new(ctx, "/dilopo/dead_2.png")?,
            graphics::Image::new(ctx, "/dilopo/dead_3.png")?,
            graphics::Image::new(ctx, "/dilopo/dead_4.png")?,
            graphics::Image::new(ctx, "/dilopo/dead_5.png")?,
            graphics::Image::new(ctx, "/dilopo/dead_6.png")?,
            graphics::Image::new(ctx, "/dilopo/dead_7.png")?,
            graphics::Image::new(ctx, "/dilopo/dead_8.png")?,
            graphics::Image::new(ctx, "/dilopo/dead_9.png")?,
            graphics::Image::new(ctx, "/dilopo/dead_10.png")?,
        ];

        Ok(Dilopo{
            run,
            walk,
            attack,
            spit,
            idle,
            dead,
            current_action: CurrentAction::IDLE,
            run_index: 0,
            walk_index: 0,
            attack_index: 0,
            spit_index: 0,
            idle_index: 0,
            dead_index: 0,
            position: Vec2::new(10.0, 710.0),
            velocity: Vec2::new(0.0, 0.0),
            venom: Vec::new()
        })
    }

    pub fn draw(&mut self, ctx: &mut Context) {
        for i in 0..self.venom.len() {
            if self.venom[i].pos.x < 1920.0 {
                self.venom[i].draw(ctx);
                self.venom[i].updatePosition();
            }
        }
    }
}

pub struct Venom {
    pos: Vec2,
    image: graphics::Image
}

impl Venom {
    pub fn new(ctx: &mut Context, pos: Vec2) -> GameResult<Venom> {
        Ok(
            Venom {
                pos,
                image: graphics::Image::new(ctx, "/dilopo/venom_1.png")?
            }
        )
    }
    
    pub fn draw(&self, ctx: &mut Context) {
        let params = graphics::DrawParam::new()
            .dest(self.pos);
        graphics::draw(ctx, &self.image, params)
            .expect("couldn't draw the damn venom");
    }

    pub fn updatePosition(&mut self) {
        self.pos.x += 7.0;
    }
}