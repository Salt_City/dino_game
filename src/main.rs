use ggez::{Context, ContextBuilder, GameResult};
use ggez::graphics::{self, Color};
use ggez::event::{self, EventHandler, KeyCode, KeyMods};
use ggez::conf;

use glam::*;

use std::env;
use std::path;
use std::time::{SystemTime};

extern crate level;

use level::Level;

mod dino;
mod level_1;

fn main() -> GameResult {
    let resource_dir = if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        path
    } else {
        path::PathBuf::from("./resources")
    };

    let (mut ctx, event_loop) = ContextBuilder::new("who_fuckin_knows", "Brandon")
        .window_setup(conf::WindowSetup::default().title("Some Shit"))
        .window_mode(conf::WindowMode::default().dimensions(1920.0, 1080.0))
        .add_resource_path(resource_dir)
        .build()
        .expect("Shit done broke");

    let a_game = Game::new(&mut ctx)?;

    event::run(ctx, event_loop, a_game);
}

struct Assets {
    dude: graphics::Image
}

impl Assets {
    fn new(_ctx: &mut Context) -> GameResult<Assets> {
        let dude = graphics::Image::new(_ctx, "/dude.png")?;
        
        let a = Assets {
            dude
        };

        Ok(a)
    }
}

struct Game {
    assets: Assets,
    dilop: dino::Dilopo,
    base_x: f32,
    base_y: f32,
    run_index: usize,
    attack: bool,
    attack_index: usize,
    frame_count: usize,
    levels: level_1::LevelOne
}

impl Game {
    pub fn new(_ctx: &mut Context) -> GameResult<Game> {
        let assets = Assets::new(_ctx)?;
        let dilop = dino::Dilopo::new(_ctx)?;

        Ok(Game {
            assets,
            dilop,
            base_x: 10.0,
            base_y: 952.0,
            run_index: 0,
            attack: false,
            attack_index: 0,
            frame_count: 0,
            levels: level_1::LevelOne::new(_ctx)
        })
    }
}

impl EventHandler<ggez::GameError> for Game {

    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        self.dilop.position += self.dilop.velocity;
        // if self.dilop.velocity.y != 0.5 {
        //     if self.dilop.position.y != 20.0 {
        //         self.dilop.position -= self.dilop.velocity;
        //     }
        //     self.dilop.velocity.y += 0.5;
        // }
        
        let params = graphics::DrawParam::new()
            .dest(Vec2::new(self.dilop.position.x, self.dilop.position.y));
        graphics::clear(ctx, Color::WHITE);
        self.levels.draw(ctx);
        self.dilop.draw(ctx);

        match self.dilop.current_action {
            dino::CurrentAction::JUMPING => {
                
                // self.base_y = (-5_f32 * (time_diff.as_secs() * time_diff.as_secs()) as f32) + (7.0_f32 * time_diff.as_secs() as f32) + 20.0_f32;
                //println!("BASE Y: {}", number);
            }
            dino::CurrentAction::IDLE => {
                graphics::draw(ctx, &self.dilop.idle[self.dilop.idle_index], params)?;
                self.dilop.idle_index = if self.dilop.idle_index == 9 {
                    0
                } else {
                    self.dilop.idle_index + 1
                };
            }
            dino::CurrentAction::WALKING => {
                graphics::draw(ctx, &self.dilop.walk[self.dilop.walk_index], params)?;
            }
            dino::CurrentAction::RUNNING => {
                graphics::draw(ctx, &self.dilop.run[self.dilop.run_index], params)?;
            }
            dino::CurrentAction::ATTACKING => {
                graphics::draw(ctx, &self.dilop.attack[self.dilop.attack_index], params)?;
                self.dilop.attack_index = if self.dilop.attack_index == 9 {
                    0
                } else {
                    self.dilop.attack_index + 1
                };
            }
            dino::CurrentAction::SPITTING => {
                graphics::draw(ctx, &self.dilop.spit[self.dilop.spit_index], params)?;
                if self.dilop.spit_index == 5 {
                    self.dilop.venom.push(dino::Venom::new(ctx, Vec2::new(self.dilop.position.x + 280.0, self.dilop.position.y + 110.0)).unwrap());
                }
                self.dilop.spit_index = if self.dilop.spit_index == 9 {
                    0
                } else {
                    self.dilop.spit_index + 1
                };
            }
        }
        graphics::present(ctx)
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keyCode: KeyCode,
        _keymod: KeyMods,
        _repeat: bool,
    ) {
        match keyCode {
            KeyCode::LShift => {
                self.dilop.current_action = dino::CurrentAction::RUNNING;
            }
            KeyCode::Left => {
                match self.dilop.current_action {
                    dino::CurrentAction::RUNNING => {
                        self.dilop.velocity.x = -5.5;
                        self.dilop.run_index =  if self.dilop.run_index == 0 {
                            7
                        } else {
                            self.dilop.run_index - 1
                        };
                    }
                    _ => {
                        self.dilop.velocity.x = -3.0;
                        self.dilop.current_action = dino::CurrentAction::WALKING;
                        self.dilop.walk_index = if self.dilop.walk_index == 0 {
                            9
                        } else {
                            self.dilop.walk_index - 1
                        };
                    }
                }
            }
            KeyCode::Right => {
                match self.dilop.current_action {
                    dino::CurrentAction::RUNNING => {
                        self.dilop.velocity.x = 5.5;
                        self.dilop.run_index = if self.dilop.run_index == 7 {
                            0
                        } else {
                            self.dilop.run_index + 1
                        };
                    }
                    _ => {
                        self.dilop.velocity.x = 3.0;
                        self.dilop.current_action = dino::CurrentAction::WALKING;
                        self.dilop.walk_index = if self.dilop.walk_index == 9 {
                            0
                        } else {
                            self.dilop.walk_index + 1
                        }; 
                    }
                }
            }
            KeyCode::A => {
                self.dilop.current_action = dino::CurrentAction::ATTACKING;
                self.dilop.walk_index = 0;
                self.dilop.run_index = 0;
            }
            KeyCode::S => {
                self.dilop.current_action = dino::CurrentAction::SPITTING;
                self.dilop.walk_index = 0;
                self.dilop.run_index = 0;
            }
            KeyCode::Space => {
                // self.dilop.current_action = dino::CurrentAction::JUMPING;
                // self.dilop.walk_index = 0;
                // self.dilop.run_index = 0;
                // self.dilop.velocity.y = -5.0;
                //self.dilop.jump_time = SystemTime::now();
            }
            _ => ()
        }
    }

    fn key_up_event(&mut self, _ctx: &mut Context, keycode: KeyCode, _keymod: KeyMods) {
        match keycode {
            _ => {
                self.dilop.velocity.x = 0.0;
                self.dilop.current_action = dino::CurrentAction::IDLE;
            }
        }
    }
}