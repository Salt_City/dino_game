use ggez::graphics;
use ggez::{Context, GameResult};

use glam::Vec2;

extern crate level;

pub struct LevelOne {
    tiles: Vec<level::Tile>,
    background: graphics::Image,
    background_param: graphics::DrawParam
}

impl level::Level for LevelOne {
    fn new(ctx: &mut Context) -> LevelOne {
        let jungle = level::JungleLevel::new(ctx)
            .expect("No jungle for your ass");

        let mut tile_counter = 0.0;
        let mut tile_vector: Vec<level::Tile> = Vec::new();

        while tile_counter <= 1920.0 {
            let tile = level::Tile::new(Vec2::new(tile_counter, 952.0),
            jungle.flat_grass_2.clone());
            tile_counter += 128.0;
            tile_vector.push(tile);
        }

        let background = graphics::Image::new(ctx, "/level/jungle/BG.png")
            .expect("couldn't find that shit!");

        let background_param = graphics::DrawParam::new()
            .dest(Vec2::new(0.0,0.0));

        LevelOne{
            tiles: tile_vector,
            background,
            background_param
        }
    }

    fn draw(&mut self, ctx: &mut Context) {
        graphics::draw(ctx, &self.background, self.background_param)
            .expect("couldn't draw background");
        
        for i in &self.tiles {
            graphics::draw(ctx, &i.image, i.param)
                .expect("couldn't draw tiles");
        }
        
    }
}