use crate::entity::Entity;
use std::collections::HashMap;

pub struct SpatialMap {
    conversion_factor: f64,
    num_rows: usize,
    num_cols: usize,
    multiplier: f64,
    spat_map: HashMap<i64, Vec<Entity>>
}

impl SpatialMap {
    pub fn new(height: usize, width: usize, cell_size: usize) -> SpatialMap {
        let conversion_factor: f64 = 1.0/cell_size as f64;
        let num_rows = height/cell_size;
        let num_cols = width/cell_size;
        // Don't always assume a 16:9 config
        let multiplier = if num_cols > num_rows {
            num_cols as f64
        } else {
            num_rows as f64
        };
        let footage = num_rows * num_cols;
        let spat_map = HashMap::with_capacity(footage);

        SpatialMap {
            conversion_factor,
            num_rows,
            num_cols,
            multiplier,
            spat_map
        }
    }

    pub fn put(&mut self, e: Entity) {
        let index = self.get_index(e);

        match self.spat_map.get_mut(&index) {
            Some(v) => v.push(e),
            None => {
                let mut v: Vec<Entity> = Vec::new();
                v.push(e);
                self.spat_map.insert(index, v);
            }
        }
    }

    pub fn get_for_entity(&mut self, e: Entity) -> Option<&Vec<Entity>> {
        let index = self.get_index(e);
        
        self.get(index)
    }

    fn get_index(&self, e: Entity) -> i64 {
        ((e.x * self.conversion_factor) as i64) + 
            ((e.y * self.conversion_factor * self.multiplier) as i64)
    }

    fn get(&self, index: i64) -> Option<&Vec<Entity>>  {
        match self.spat_map.get(&index) {
            Some(v) => Some(v),
            None => None
        }
    }


}